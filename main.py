from flask import Flask, render_template
from os import listdir
from os.path import join, isfile
import json

import querytexts as qt

app = Flask(__name__)

files_dir = './files'

files_for_index = [join(files_dir, f) for f in listdir(files_dir) if isfile(join(files_dir, f))]

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/search/<string:querys>')
def search(querys):
	index = qt.Query(files_for_index)
	result = index.free_text_query(querys)
	return json.dumps(result)
