FROM ubuntu:18.04

WORKDIR /app

RUN apt-get update \
	&& apt-get install -y python3 \
	&& apt-get install -y python3-pip \
	&& pip3 install flask

COPY ./templates ./templates
COPY ./static ./static
COPY ./files ./files
COPY stopwords.txt ./
COPY buildindex.py ./
COPY querytexts.py ./
COPY main.py ./

ENV LANG C.UTF-8
ENV FLASK_APP /app/main.py

CMD ["python3", "-m", "flask", "run"]
